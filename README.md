# MPDND #

### Summary ###

MPDND (Music Player Daemon Notification Daemon) displays desktop notifications for MPD events.

![screenshot](scrot.png)

### Usage ###

Add mpdnd to ~/.xinitrc. Run `mpdnd -h` for a list of options.

Note that MPDND is still a work in progress. Currently, for example, only album cover art images named "cover.jpg/jpeg/png" or "Folder.jpg/jpeg/png" are recognized and there are likely to be bugs not yet spotted.

### Installation ###

`make && make install`

### Dependencies and requirements ###

- libgtk-pixbuf
- libmpdclient
- libnotify
- a running instance of MPD
- a notification server; I myself use Dunst.

### TODO ###

- recognize any image file as cover art
- fix events being missed when switching tracks quickly
- possibly limit notification body length to a couple of lines (for albums like Prajecyrujučy Sinhuliarnaje Wypramieńwańnie Daktryny Absaliutnaha J Usiopahłynaĺnaha Zła Skroź Šaścihrannuju Pryzmu Sîn​-​Ahhī​-​Erība Na Hipierpawierchniu Zadyjakaĺnaha Kaŭčęha Zasnawaĺnikaŭ Kosmatęchničnaha Ordęna Palieakantakta​.​.​.)
- add more customization options

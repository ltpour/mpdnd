/* 
   MPDND: Music Player Daemon Notification Daemon

   Copyright 2018 Lasse Pouru

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <syslog.h>
#include <unistd.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libnotify/notify.h>
#include <mpd/client.h>

#define PROGRAM_NAME "MPDND"
#define PROGRAM_VERSION "0.1"
#define DEFAULT_ICON "/usr/local/share/mpdnd/mpdnd.png"
#define ICON_SIZE 64

static volatile sig_atomic_t quit = 0;
static volatile pid_t pid;

/* Option variables */
static int minimal;
static const char *default_icon, *host, *music_directory;
static unsigned port;
static const struct option longopts[] = {
    {"dir",     required_argument, NULL, 'd'},
    {"help",    no_argument,       NULL, 'h'},
    {"icon",    required_argument, NULL, 'i'},
    {"minimal", no_argument,       NULL, 'm'},
    {"port",    required_argument, NULL, 'p'},
    {"host",    required_argument, NULL, 's'},
    {"version", no_argument,       NULL, 'v'},
    {NULL,      0,                 NULL, 0 }};

/* Prints help */
static void help (char* prgname) {
    printf("Usage: %s [OPTION]...\n", prgname);
    printf("Show MPD event notifications.\n\n");
    printf("  -d, --dir\tmpd music directory; defaults to $HOME/Music\n");
    printf("  -i, --icon\tdefault icon; defaults to %s\n", DEFAULT_ICON);
    printf("  -m, --minimal\tdon't show pause and stop notifications\n");
    printf("  -p, --port\tmpd port; defaults to MPD_PORT\n");
    printf("  -s, --host\tmpd host or socket;lts to MPD_HOST\n");
    printf("  -v, --version\tshow version information and exit\n");
    printf("  -h, --help\tshow this help and exit\n");
}

/* Prints program version */
static void version (char* prgname) {
    printf("%s %s\n", PROGRAM_NAME, PROGRAM_VERSION);
    printf("Copyright 2018 Lasse Pouru\n");
    printf("License GPLv3: GNU GPL version 3 <http://gnu.org/licenses/gpl.html>\n");
}

/* Sets quit flag for exiting main loop */
static void termination_handler(int signum) {
    switch(signum) {
    case SIGINT:
    case SIGTERM:
    case SIGSEGV:
	quit = 1;
    default:
	break;
    }
}

/* Returns pid of new daemon */
static pid_t daemonize() {

    /* Fork and exit parent */
    pid_t pid = fork();
    if (pid < 0) {
	syslog(LOG_ERR, "%s: could not fork: %m", PROGRAM_NAME);
	closelog();
	exit(EXIT_FAILURE);
    } else if (pid > 0)
	exit(EXIT_SUCCESS);

    /* Create a new SID for the child process */
    if (setsid() < 0) {
	syslog(LOG_ERR, "%s: could not create a session ID: %m", PROGRAM_NAME);
	closelog();
	exit(EXIT_FAILURE);
    }

    /* Set signal handlers */
    signal(SIGHUP, termination_handler);
    signal(SIGINT, termination_handler);
    signal(SIGTERM, termination_handler);
    signal(SIGSEGV, termination_handler);
    
    /* Fork and exit parent again */
    pid = fork();
    if (pid < 0) {
	syslog(LOG_ERR, "%s: could not fork: %m", PROGRAM_NAME);
	closelog();
	exit(EXIT_FAILURE);
    } else if (pid > 0)
	exit(EXIT_SUCCESS);
    
    /* Change the working directory to root */
    if ((chdir("/")) < 0) {
	syslog(LOG_ERR, "%s: could not change working directory: %m", PROGRAM_NAME);
	closelog();
	exit(EXIT_FAILURE);
    }

    /* Change the file mode mask */
    umask(0);

    /* Close all open file descriptors */
    for (int fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--)
	close (fd);

    /* Return daemon pid */
    return getpid();
}

/* Sets customizable variables */
static void parse_opts (int argc, char* argv[]) {

    /* Set option flags */
    const char *dir_arg;
    int c = 0, d = 0, h = 0, i = 0, m = 0, p = 0, s = 0, v = 0;
    while((c = getopt_long(argc, argv, "ad:hi:p:s:v", longopts, NULL)) != -1) {
	switch (c) {
	case 0:
	    break;
	case 'd': // dir
	    dir_arg = optarg;
	    if (dir_arg && dir_arg[0] == '~')
		music_directory = strncat(getenv("HOME"), &dir_arg[1], strlen(dir_arg));
	    else
		music_directory = dir_arg;
	    d = 1;
	    break;
	case 'h': // help
	    h = 1;
	    break;
	case 'i': // icon
	    default_icon = optarg;
	    i = 1;
	    break;
	case 'm': // minimal
	    m = 1;
	    break;
	case 'p': // port
	    port = atoi(optarg);
	    p = 1;
	    break;
	case 's': // host
	    host = optarg;
	    s = 1;
	    break;
	case 'v': // version
	    v = 1;
	    break;
	default:
	    printf("Try '%s --help' for more information.\n", argv[0]);
	    exit(EXIT_FAILURE);
	}
    }

    /* Assign variables or display help or version information */
    minimal = m;
    if (h) {
	help(argv[0]);
	exit(EXIT_SUCCESS);
    }
    if (v) {
	version(argv[0]);
	exit(EXIT_SUCCESS);
    }
    if (!d)
	music_directory = strncat(getenv("HOME"), "/Music", 7);
    if (!i)
	default_icon = DEFAULT_ICON;
    if (!p)
	port = 0;
    if (!s)
	host = NULL;
    return;
}

int main(int argc, char *argv[]) {
    
    /* Parse options */
    parse_opts(argc, argv);

    /* Open log */
    openlog(PROGRAM_NAME, LOG_ODELAY, LOG_DAEMON);

    /* Become daemon */
    pid = daemonize();

    /* Open a connection to MPD server and initialize libnotify */
    struct mpd_connection *connection = mpd_connection_new(host, port, 0);
    int retries = 10;
    while (mpd_connection_get_error(connection) != MPD_ERROR_SUCCESS) {
	mpd_connection_free(connection);
	retries--;
	if (retries == 0) {
	    syslog(LOG_ERR, "%s: could not connect to MPD server: %m", PROGRAM_NAME);
	    closelog();
	    exit(EXIT_FAILURE);
	}
	sleep(1);
	connection = mpd_connection_new(host, port, 0);
    }
    retries = 10;
    while (!notify_init(PROGRAM_NAME)) {
	retries--;
	if (retries == 0) {
	    mpd_connection_free(connection);
	    syslog(LOG_ERR, "%s: failed to initialize libnotify: %m", PROGRAM_NAME);
	    closelog();
	    exit(EXIT_FAILURE);
	}
	sleep(1);
    }
    
    /* Main loop */
    NotifyNotification *notification = notify_notification_new(NULL, NULL, NULL);
    while(mpd_run_idle_mask(connection, MPD_IDLE_PLAYER) && !quit) {

	/* Send command list */
	mpd_command_list_begin(connection, true);
	mpd_send_status(connection);
	mpd_send_current_song(connection);
	mpd_command_list_end(connection);

	/* Receive status */
	struct mpd_status *status = mpd_recv_status(connection);
	enum mpd_state state = mpd_status_get_state(status);
	char notification_summary[9];
	if (minimal) {
	    strncpy(notification_summary, " ", 1);
	    notification_summary[1] = '\0';
	} else {
	    switch (state) {
	    case MPD_STATE_PLAY:
		strncpy(notification_summary, "Playing:", 8);
		notification_summary[8] = '\0';
		break;
	    case MPD_STATE_PAUSE:
		strncpy(notification_summary, "Paused:", 7);
		notification_summary[7] = '\0';
		break;
	    case MPD_STATE_STOP:
		strncpy(notification_summary, "Stopped:", 8);
		notification_summary[8] = '\0';
		break;
	    default:
		strncpy(notification_summary, "ERROR:", 6);
		notification_summary[6] = '\0';
	    }
	}   

	/* Receive song */
	mpd_response_next(connection);
	struct mpd_song *song = mpd_recv_song(connection);

	/* Update notification */
	if(song) {
	    if (!minimal || state == MPD_STATE_PLAY) {		
		const char *artist = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0) ? mpd_song_get_tag(song, MPD_TAG_ARTIST, 0) : ""; 
		const char *title = mpd_song_get_tag(song, MPD_TAG_TITLE, 0) ? mpd_song_get_tag(song, MPD_TAG_TITLE, 0) : "";
		const char *song_uri = mpd_song_get_uri(song);
		const char *song_filename_start = strrchr(song_uri, '/');
		const char *album_cover_candidates[] = {"cover.jpg", "cover.jpeg", "cover.png", "Folder.jpg", "Folder.jpeg", "Folder.png"};
		
		/* Construct album cover path */
		char album_directory[strlen(song_uri) + 1]; // newline
		memset (album_directory, 0, sizeof album_directory);
		strncpy(album_directory, song_uri, strlen(song_uri) - strlen(song_filename_start) + 1);

		char album_cover_path[strlen(music_directory) + strlen(album_directory) + 2]; // newline + separator
		memset (album_cover_path, 0, sizeof album_cover_path);
		snprintf(album_cover_path, sizeof album_cover_path, "%s/%s", music_directory, album_directory);

		bool cover_found = false;
		char album_cover_full_path[strlen(album_cover_path) + 12]; // newline + longest cover candidate
		int i = 0;
		while(!cover_found && i < 6) {
		    memset (album_cover_full_path, 0, sizeof album_cover_full_path);
		    strcpy(album_cover_full_path, album_cover_path);
		    strncat(album_cover_full_path, album_cover_candidates[i], 10);
		    if (access (album_cover_full_path, F_OK) != -1)
			cover_found = true;
		    i++;
		}
		
		/* Construct notification body */
		char notification_body[strlen(artist) + strlen(title) + 4]; // newline + separators
		snprintf(notification_body, sizeof notification_body, "%s - %s", artist, title);

		/* Update notification image */
		GError *gerror = NULL;		
		GdkPixbuf *icon;
		
		if (access (album_cover_full_path, F_OK) != -1)
		    icon = gdk_pixbuf_new_from_file_at_scale(album_cover_full_path, ICON_SIZE, ICON_SIZE, TRUE, &gerror);
		else
		    icon = gdk_pixbuf_new_from_file(DEFAULT_ICON, &gerror);
		notify_notification_update(notification, notification_summary, notification_body, NULL);
		notify_notification_set_image_from_pixbuf(notification, icon);
		gerror = NULL;

		/* Show notification */		
		notify_notification_show(notification, &gerror);
	    }
	    mpd_song_free(song);
	}	
	mpd_response_finish(connection);
    }

    /* Exit */
    mpd_connection_free(connection);
    notify_uninit();
    closelog();
    exit(EXIT_SUCCESS);
}

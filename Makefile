PROG	   = mpdnd
CC	   = gcc
PREFIX	  ?= /usr/local

CFLAGS	= -Wall
LIBS	= -lmpdclient
CFLAGS	+= `pkg-config --cflags libnotify gdk-pixbuf-2.0`
LIBS	+= `pkg-config --libs libnotify gdk-pixbuf-2.0`

${PROG}: ${PROG}.c
	@${CC} ${CFLAGS} ${LIBS} -o ${PROG} ${PROG}.c
	@strip ${PROG}

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/share/${PROG}
	install -Dm755 ${PROG} ${DESTDIR}${PREFIX}/bin/${PROG}
	cp mpdnd.png ${DESTDIR}${PREFIX}/share/${PROG}/

uninstall:
	rm -f ${PREFIX}/bin/${PROG}
	rm -f ${PREFIX}/share/${PROG}/mpdnd.png

clean:
	rm -f ${PROG}
	rm -f ${PREFIX}/share/${PROG}/mpdnd.png
